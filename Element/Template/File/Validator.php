<?php

namespace GetNoticed\Developer\Element\Template\File;

use GetNoticed\Developer\Helper\Helper;

class Validator
{

    /**
     * @var Helper
     */
    protected $developerHelper;

    public function __construct(Helper $developerHelper)
    {
        $this->developerHelper = $developerHelper;
    }


    public function aroundIsValid(\Magento\Framework\View\Element\Template\File\Validator $validator, \Closure $proceed, $filename)
    {
        if (false === $this->developerHelper->isDeveloperMode()) {
            return $proceed($filename);
        }

        return true;
    }

}