<?php

namespace GetNoticed\Developer\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\State;

class Helper extends AbstractHelper
{

    /**
     * @var State
     */
    protected $state;

    public function __construct(Context $context, State $state)
    {
        $this->state = $state;

        parent::__construct($context);
    }


    public function isDeveloperMode()
    {
        return $this->state->getMode() == State::MODE_DEVELOPER;
    }

}