<?php

namespace GetNoticed\Developer\Console\Command\Generate\Module;

use Magento\Framework;
use GetNoticed\Common;
use Symfony\Component\Console;

class SkeletonCommand
    extends Common\Console\Command\AbstractCommand
{

    /**
     * @var string
     */
    const GIT_CMD_SKELETON = 'git clone -b %s git@bitbucket.org:GetNoticed/m2-module-empty-skeleton-module.git .';

    /**
     * @var string
     */
    const GIT_DEFAULT_BRANCH = 'master';

    /**
     * @var string
     */
    protected $vendorNamespaceCC = 'GetNoticed';

    /**
     * @var string
     */
    protected $vendorNamespaceLC = 'getnoticed';

    /**
     * @var string
     */
    protected $moduleNamespaceCC;

    /**
     * @var string
     */
    protected $moduleNamespaceLC;

    /**
     * @var string
     */
    protected $codeDirectory;

    /**
     * @var string
     */
    protected $moduleDirectory;

    /**
     * @var string
     */
    protected $gitBranch = self::GIT_DEFAULT_BRANCH;

    // DI

    /**
     * @var Framework\Filesystem\DirectoryList
     */
    protected $directoryList;

    public function __construct(
        Framework\Filesystem\DirectoryList $directoryList,
        string $name
    ) {
        $this->directoryList = $directoryList;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setDescription('Generate a module based on the Get.Noticed skeleton.');
        $this->setDefinition(
            new Console\Input\InputDefinition(
                [
                    new Console\Input\InputOption(
                        'vendor-cc',
                        null,
                        Console\Input\InputOption::VALUE_OPTIONAL,
                        'Enter your vendor namespace (camelcased, "GetNoticed" by default)',
                        'GetNoticed'
                    ),
                    new Console\Input\InputOption(
                        'vendor-lc',
                        null,
                        Console\Input\InputOption::VALUE_OPTIONAL,
                        'Enter your vendor namespace (lowercase, "getnoticed" by default)',
                        'getnoticed'
                    ),
                    new Console\Input\InputOption(
                        'module-cc',
                        null,
                        Console\Input\InputOption::VALUE_REQUIRED,
                        'Enter your module namespace (camelcased)'
                    ),
                    new Console\Input\InputOption(
                        'module-lc',
                        null,
                        Console\Input\InputOption::VALUE_REQUIRED,
                        'Enter your module namespace (lowercase)'
                    ),
                    new Console\Input\InputOption(
                        'skeleton-branch',
                        's',
                        Console\Input\InputOption::VALUE_OPTIONAL,
                        'Use a different branch for the skeleton repository',
                        self::GIT_DEFAULT_BRANCH
                    )
                ]
            )
        );
    }

    protected function execute(
        Console\Input\InputInterface $input,
        Console\Output\OutputInterface $output
    ) {
        parent::execute($input, $output);

        try {
            // Check if another git branch is chosen
            $gitBranch = trim($this->input->getOption('skeleton-branch'));

            if (strlen($gitBranch) > 0) {
                $this->gitBranch = $gitBranch;
            }

            // Validate and set options
            $this->validateOptions();

            $this->vendorNamespaceCC = trim($this->input->getOption('vendor-cc'));
            $this->vendorNamespaceLC = trim($this->input->getOption('vendor-lc'));
            $this->moduleNamespaceCC = trim($this->input->getOption('module-cc'));
            $this->moduleNamespaceLC = trim($this->input->getOption('module-lc'));

            // Set and validate target
            $this->codeDirectory = $this->directoryList->getPath('app') . DIRECTORY_SEPARATOR . 'code';
            $this->moduleDirectory = $this->codeDirectory . DIRECTORY_SEPARATOR . $this->vendorNamespaceCC . DIRECTORY_SEPARATOR . $this->moduleNamespaceCC;

            $this->validateTarget();
            $this->createModuleDirectory();

            // Download and prepare skeleton
            $this->prepareSkeleton();
            $this->replacePlaceholders();

            // Done
            $output->writeln('<info>Module was successfully created.</info>');
        } catch (\Exception $e) {
            $output->writeln(sprintf('<error>Error: %s</error>', $e->getMessage()));
        }
    }

    private function validateOptions()
    {
        // Vendor (camelcase)
        if (strlen(trim($this->input->getOption('vendor-cc'))) < 1) {
            throw new Console\Exception\InvalidArgumentException('Vendor name (camelcase) is required');
        }

        // Vendor (lowercase)
        if (strlen(trim($this->input->getOption('vendor-lc'))) < 1) {
            throw new Console\Exception\InvalidArgumentException('Vendor name (lowercase) is required');
        }

        // Module (camelcase)
        if (strlen(trim($this->input->getOption('module-cc'))) < 1) {
            throw new Console\Exception\InvalidArgumentException('Module name (camelcase) is required');
        }

        // Module (lowercase)
        if (strlen(trim($this->input->getOption('module-lc'))) < 1) {
            throw new Console\Exception\InvalidArgumentException('Module name (lowercase) is required');
        }
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function validateTarget()
    {
        if (\is_dir($this->moduleDirectory) === true) {
            $filesInDir = glob($this->moduleDirectory . DIRECTORY_SEPARATOR . '*');

            if (is_array($filesInDir) && count($filesInDir) > 0) {
                throw new Framework\Exception\LocalizedException(
                    __('Target directory already exists and is not empty, aborting.')
                );
            }
        }
    }

    private function createModuleDirectory()
    {
        if (\is_dir($this->moduleDirectory) !== true) {
            \mkdir($this->moduleDirectory, 0755, true);
        }
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function prepareSkeleton()
    {
        if (chdir($this->moduleDirectory) === false) {
            throw new Framework\Exception\LocalizedException(__('Unable to change working directory'));
        }

        shell_exec(sprintf(self::GIT_CMD_SKELETON, $this->gitBranch));

        if (file_exists($this->moduleDirectory . DIRECTORY_SEPARATOR . '.git/config') !== true) {
            throw new Framework\Exception\LocalizedException(
                __('Failed to checkout skeleton module - please check the error(s) above.')
            );
        }

        shell_exec('rm -rf .git && git init');
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function replacePlaceholders()
    {
        $this->getDirContents($this->moduleDirectory, $files, ['.git']);

        if (!is_array($files)) {
            throw new Framework\Exception\LocalizedException(
                __('Something went wrong while transforming files.')
            );
        }

        foreach ($files as $file) {
            $this->transformFile($file);
        }
    }

    private function transformFile(string $filePath)
    {
        // Get file contents
        $fileContents = \file_get_contents($filePath);

        // Do not process empty files
        if (strlen($fileContents) < 1) {
            return;
        }

        // Generate new file
        $fileContents = strtr(
            $fileContents,
            [
                'GetNoticed'   => $this->vendorNamespaceCC,
                'getnoticed'   => $this->vendorNamespaceLC,
                'EmptyModule'  => $this->moduleNamespaceCC,
                'empty-module' => $this->moduleNamespaceLC,
                'emptymodule'  => strtr($this->moduleNamespaceLC, ['-' => '']),
                'Empty Module' => sprintf('%s %s', $this->vendorNamespaceCC, $this->moduleNamespaceCC)
            ]
        );

        \file_put_contents($filePath, $fileContents);
    }

    private function getDirContents($dir, &$results = [], $exclude = [])
    {
        $files = scandir($dir);

        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            } else {
                if (!in_array($value, array_merge(['.', '..'], $exclude))) {
                    $this->getDirContents($path, $results);
                }
            }
        }

        return $results;
    }

}
